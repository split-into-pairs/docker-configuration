# SplitIntoGroups

This is a basic application built on React as a client application and Go as a server application. Purpose of this application is to take N elements with different preferences (there is Employee, which has described where he lives, in what team in job he works, what age he has and is he have any problem with eyesight) and make matching pairs where each preference cannot be the same for two employees.

Algorithm used to make this application is known as _backtracing_.

## What is _backtracing_?

> Backtracking is a general algorithm for finding all (or some) solutions to some computational problems, that incrementally builds candidates to the solutions, and abandons each partial candidate (“backtracks”) as soon as it determines that the candidate cannot possibly be completed to a valid solution.

Recommended sources for more information:

* [Backtracking explained](https://medium.com/@andreaiacono/backtracking-explained-7450d6ef9e1a)
* [Recursive Backtracking Explanation](https://www.cs.utexas.edu/~scottm/cs307/handouts/recursiveBacktrackingExplanation.htm)